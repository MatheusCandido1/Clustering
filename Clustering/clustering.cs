﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Clustering;
using System.Data;

namespace Clustering
{
    class clustering
    {

        dbcontext objDAL = new dbcontext();

        public string NAME { get; set; }
        public double MDVPFoHz { get; set; }
        public double MDVPFhiHz { get; set; }
        public double MDVPFloHz { get; set; }
        public double MDVPJitter { get; set; }
        public double MDVPJitterAbs { get; set; }
        public double MDVPRAP { get; set; }
        public double MDVPPPQ { get; set; }
        public double JitterDDP { get; set; }
        public double MDVPShimmer { get; set; }
        public double MDVPShimmerdB { get; set; }
        public double ShimmerAPQ3 { get; set; }
        public double ShimmerAPQ5 { get; set; }
        public double MDVPAPQ { get; set; }
        public double ShimmerDDA { get; set; }
        public double NHR { get; set; }
        public double HNR { get; set; }
        public int status { get; set; }
        public double RPDE { get; set; }
        public double DFA { get; set; }
        public double spread1 { get; set; }
        public double spread2 { get; set; }
        public double D2 { get; set; }
        public double PPE { get; set; }

        public DataTable ReturnData()
        {
            objDAL.Conectar();
            DataTable data = new DataTable();
            string sql = String.Format("SELECT '{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}','{21}','{22}'" +
            "FROM MYTABLE", MDVPFoHz, MDVPFhiHz, MDVPFloHz, MDVPJitter, MDVPJitterAbs, MDVPRAP, MDVPPPQ, JitterDDP, MDVPShimmer, MDVPShimmerdB, ShimmerAPQ3, ShimmerAPQ5, MDVPAPQ, ShimmerDDA, NHR, HNR, status, RPDE, DFA, spread1, spread2, D2, PPE);
            data = objDAL.RetDataTable(sql);
            return data;
        }

        public DataTable DataReturn()
        {
            objDAL.Conectar();
            DataTable data = new DataTable();
            data = objDAL.RetDataTable("Select * from mytable");
            return data;
        }

    }
}
