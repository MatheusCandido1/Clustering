﻿using System;
using System.Data;
using System.Collections;


namespace Clustering
{

    public class Program
    {

        // Cumulative Sum Function
        static double[] CumSum(double[] arr)
        {
            double[] x = new double[arr.Length];
            for(int i = 0; i <= arr.Length - 1; i++)
            {
                if(i == 0)
                {
                    x[i] = arr[i];
                }
                else
                {
                    x[i] = x[i - 1] + arr[i];
                }              
            }

            return x;
        }

        static void Main(string[] args)
        {
            // Creating vectors 
            double[,] park = new double[24, 195];
            double[] first = new double[24];
            double[,] sim = new double[195, 195];
            double[,] mar = new double[2, 195];
            double[] test = new double[195];
            double[,] mar2 = new double[2, 195];
            double[,] third = new double[195, 195];
            double[] secondV = { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            double[] secondV2 = new double[24];
            double[] secondV3 = new double[24];
            double[] marginV = new double[24];
            clustering obj = new clustering();
            DataTable data = obj.DataReturn();
            int j = 0;


            // Looping the data;
            for (j = 0; j <= 194; j++)
            {
                park[0, j] = double.Parse(data.Rows[j]["MDVPFoHz"].ToString());
                park[1, j] = double.Parse(data.Rows[j]["MDVPFhiHz"].ToString());
                park[2, j] = double.Parse(data.Rows[j]["MDVPFloHz"].ToString());
                park[3, j] = double.Parse(data.Rows[j]["MDVPJitter"].ToString());
                park[4, j] = double.Parse(data.Rows[j]["MDVPJitterAbs"].ToString());
                park[5, j] = double.Parse(data.Rows[j]["MDVPRAP"].ToString());
                park[6, j] = double.Parse(data.Rows[j]["MDVPPPQ"].ToString());
                park[7, j] = double.Parse(data.Rows[j]["JitterDDP"].ToString());
                park[8, j] = double.Parse(data.Rows[j]["MDVPShimmer"].ToString());
                park[9, j] = double.Parse(data.Rows[j]["MDVPShimmerdB"].ToString());
                park[10, j] = double.Parse(data.Rows[j]["ShimmerAPQ3"].ToString());
                park[11, j] = double.Parse(data.Rows[j]["ShimmerAPQ5"].ToString());
                park[12, j] = double.Parse(data.Rows[j]["MDVPAPQ"].ToString());
                park[13, j] = double.Parse(data.Rows[j]["NHR"].ToString());
                park[14, j] = double.Parse(data.Rows[j]["HNR"].ToString());
                park[15, j] = double.Parse(data.Rows[j]["status"].ToString());
                park[16, j] = double.Parse(data.Rows[j]["RPDE"].ToString());
                park[17, j] = double.Parse(data.Rows[j]["DFA"].ToString());
                park[18, j] = double.Parse(data.Rows[j]["spread1"].ToString());
                park[19, j] = double.Parse(data.Rows[j]["spread2"].ToString());
                park[20, j] = double.Parse(data.Rows[j]["D2"].ToString());
                park[21, j] = double.Parse(data.Rows[j]["MDVPFoHz"].ToString());
                park[22, j] = double.Parse(data.Rows[j]["MDVPFoHz"].ToString());
                park[23, j] = double.Parse(data.Rows[j]["PPE"].ToString());

            }
            /* // Similarities

            int i = 0;
            int x = 0;
            int w = 0;
            int p = 0;
            double sum = 0;
            for (p = 0; p <= 194; p++)
            {
                for (x = 0; x <= 194; x++)
                {
                    sum = 0;
                    for (i = 0; i <= 23; i++)
                    {
                        first[i] = Math.Pow(double.Parse(park[i, x].ToString("F6")) - double.Parse(park[i, p].ToString("F6")), 2);
                        sum = sum + first[i];
                        if (i == 23)
                            for (w = 0; w <= 23; w++)
                            {
                                first[w] = 0;
                            }
                        sim[x, p] = Math.Sqrt(sum);
                        sim[p, x] = Math.Sqrt(sum);
                    }
                }
            }
            */

            // Marginal //

            int a = 0; int b = 0; int c = 0; int d = 0; int e = 0; 
            double sum2 = 0;
            double sum3 = 0;
            double aux = 0;
            double[] sumVector = new double[24];

            for (c = 0; c <= 23; c++)
            {
                secondV2[c] = secondV[c];
                Math.Pow(secondV[c], 2);
                sum2 = sum2 + secondV[c];
            }

            Math.Sqrt(sum2);


            for (c = 0; c <= 23; c++)
            {
                sumVector[b] = secondV3[c] = secondV2[c] / sum2;

            }

            for (a = 0; a <= 194; a++)
            {
                sum3 = 0;
                for (b = 0; b <= 23; b++)
                {
                    sumVector[b] = park[b, a] * secondV3[b];
                    sum3 = sum3 + sumVector[b];
                    if (b == 23)
                    {
                        mar[0, a] = sum3;
                        for (d = 0; d <= 23; d++)
                        {
                            sumVector[b] = 0;
                        }
                    }
                }
            }


            for (e = 0; e <= 194; e++)
            {
                mar[1, e] = e;
            }


            for (e = 0; e <= 194; e++)
            {
                test[e] = mar[0, e];
            }

           

            
           for (int i = 0; i <= 194; i++)
            {
                for (int f = 0; f <= 194; f++)
                {
                    if (test[i] < test[f])
                    {
                        aux = test[i];
                        test[i] = test[f];
                        test[f] = aux;
                    }
                }
            }


            for (e = 0; e <= 194; e++)
            {
                for (int g = 0; g <= 194; g++)
                {
                    if(test[g] == mar[0,e])
                    {
                        mar2[0, e] = test[g];
                        mar2[1, e] = mar[1, g];
                    }
                }
            }

            for (int i = 0; i <= 194; i++)
            {
                for (int f = 0; f <= 194; f++)
                {
                    if (mar2[0,i] < mar2[0,f])
                    {
                        aux = mar2[0,i];
                        mar2[0,i] = mar2[0,f];
                        mar2[0,f] = aux;
                    }
                }
            }

 

            

            // Avg Points //
/*
            double[] avgPoints = new double[194];

            for (e = 0; e <= 193; e++)
            {
                avgPoints[e] = (mar2[0, e] + mar2[0, e + 1]) / 2;
            }*/

            // ********* //

            // Cumulative sum //

         /*   double[] sumCum = new double[194];
            for (e = 0; e <= 193; e++)
            {
                if (e == 193)
                {
                    sumCum[e] = avgPoints[e] + avgPoints[e];
                    break;

                }
                if (e == 0)
                {
                    sumCum[e] = avgPoints[e];
                }
                else
                {
                    sumCum[e] = avgPoints[e] + avgPoints[e+1];
                }
                
            }
            
            for (e = 0; e <= 193; e++)
            {
                Console.WriteLine(sumCum[e].ToString("F6"));
            }*/

            // ********* //

            // Exp Function //

            double[] Exp = new double[195];

            for(e = 0; e <= 194; e++)
            {
                if (e == 0)
                {
                    Exp[e] = mar2[0, 0] - mar2[0, 0];
                }
                Exp[e] = mar2[0, 0] - mar2[0, e];
                Exp[e] = Math.Exp(Exp[e]);
            }

            // *********** //

            //     EG     //
            double[] EG = new double[195];
            double[] EG2 = new double[195];
            double[] EG3 = new double[195];
            double[] EG4 = new double[195];
            double[] EG5 = new double[195];
            double[] EG6 = new double[195];
            double[] EG5sum = new double[195];
            double[] EG6sum = new double[195];
            double[] EGnegative = new double[195];
            
            
            for (e = 0; e <= 194; e++)
            {
                EG[e] = (1 / Exp[e]);
            }

            EG2 = CumSum(EG);
            EG3 = CumSum(Exp);
            

            for(e = 0; e <= 194; e++)
            {
                EGnegative[e] = (-Exp[e]);
            }

            for (e = 0; e <= 194; e++)
            {
                EG4[e] = EGnegative[e] + EG3[194];
            }

            for (e = 0; e <= 194; e++)
            {
                EG5[e] = (Exp[e] / EGnegative[e]);
            }


            for (e = 0; e <= 194; e++)
            {
                EG6[e] = Exp[e] * EG3[e];
            }

            EG5sum = CumSum(EG5);
            EG6sum = CumSum(EG6);

            double[] k = new double[195];
            double[] n = new double[195];
            double[] o = new double[195];
            double[] v = new double[195];

            for(e = 0; e <= 194; e++)
            {
                k[e] = EG[e] * (EG3[e] - EG[194]);
                n[e] = EG5sum[e] + EG6sum[e];
                o[e] = (EG5sum[194] - EG5sum[e]) + (EG6sum[194] - EG6sum[e]);
                v[e] = k[e] * ((1 / n[e]) + (1 / o[e]));
            }

            double[] E = new double[195];
            double[] oneE = new double[195];
            double[] E2 = new double[195];
            double[] E3 = new double[195];


            for (e = 0; e <= 194; e++)
            {
                E[e] = mar2[0, e] - mar2[0, 0];
                E[e] = Math.Exp(E[e]);
            }

            for(e = 0; e <= 194; e++)
            {
                oneE[e] = (1/E[e]);
            }


            // Creating Parcials
            E2 = CumSum(oneE);
            double ParcialSum1 = 0;
            double ParcialSum2 = 0;
            double ParcialSum3 = 0;

            E3 = CumSum(E);


            ParcialSum1 = E[0] * (E2[194] - (E2[0]));
            ParcialSum2 = ParcialSum1;
            ParcialSum3 = ParcialSum1;

            double[] ParcialOne = new double[195];
            double[] ParcialTwo = new double[195];
            double[] ParcialThree = new double[195];

            for (e = 0; e <= 194; e++)
            {
                if(e == 0)
                {
                    ParcialOne[0] = ParcialSum1;
                }
                else
                {
                    ParcialOne[e] = (E3[0] / E[e]);
                    ParcialOne[e] =(-1 * ParcialOne[e]);
                }
                
            }

            for (e = 0; e <= 194; e++)
            {
                if (e == 0)
                {
                    ParcialTwo[0] = ParcialSum2;
                }
                else
                {
                    ParcialTwo[e] = (E3[0] / E[e]);
                    ParcialTwo[e] = (-1 * ParcialTwo[e]);
                }

            }

            for (e = 0; e <= 194; e++)
            {
                if (e == 0)
                {
                    ParcialThree[0] = ParcialSum3;
                }
                else
                {
                    ParcialThree[e] = (2 * ((oneE[194] - oneE[e]))) - (2 * (E3[e-1] - E3[0] / E[e])) - (E3[0] / E[e]) ;
                }
            }

            double[] ParcialDerivative = new double[195];


            for(e = 0; e <= 194; e++)
            {
                ParcialDerivative[e] =((ParcialOne[e] * ((1 / ParcialSum2) + (1 / ParcialSum3))) - (k[0] * ((ParcialTwo[e]/Math.Pow(o[0],2)) + (ParcialThree[e] / Math.Pow(ParcialSum3, 2)))));
            }
            


            // *********** //



            Console.ReadKey();

            

            }

      

      
    }
    }


